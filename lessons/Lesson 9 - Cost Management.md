# Lesson 9

## AWS Calculators
* AWS Simple Monthly Calculator
    * Service Pricing models
* AWS Total Cost of OWnership
    * Raw Resources difference between doing your own server vs Amazon

## Cost Management Tools
* AWS Cost Explorer
    * View your costs of AWS

### AWS Cost and Usage Report
* Access Highly detailed billing infromation
* CSV files saved to S3 Bucket
* Ingest reports inot Redshift or Quicksight
* Usage isted for each service
* Usage listed for tags
* Can aggrgate to daily or monthly

## AWS Trusted Advisor
* Authmatically Analyzes Environment
* Offers best practice recommendations
* Cost optimization
* Security
* Fault Tolerance

### Seven Core checks
* S3 Bucket Permissions
* Security Groups (Specfic ports unrestricted
* IAM Use
* MFA on Root Account
* EBS Public Snapshots
* RDS Public Snapshots
* Service Limits

### Added Benfinits for higher level
* Notficiations and emails

## AWS Support
### Basic Support
* Core Trusted Advisor Checks
* No Technical Support
* Can submit
    * Bugs
    * Feature requests
    * Service limit increases

### DEveloper Support
* Core Trusted advisor checks
* Business hours access to cloud support Associates
* Guidance < 24 business hours
* Impairments < 12 business hours
* * Only offers general guidence

### Buisness Support
* Full Set of trusted advisor checks
* 24/7 access to cloud support engineers 
* Email char phone
* 1-24 hour response
* Offers contextual guidances.

### Enterprise support.
* Full set of trusted advisor checks
* 24/7 access to Cloud Support Engineers
* Email, Chat, Phone
* 15 min to 24 hour response
* offers consultative review based on use-case
* Applies to all accounts
* Access to well-architected review
* Access to online labs
* Technical Account manager
