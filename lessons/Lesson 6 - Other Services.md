# Lesson 6

## Importance of Automation
* Fast & efficient
* Reliable, consistent
* Repeatable
* Documentation is inherent
* Much more Secure

### Tools
* AWS
    * AWS CLI
    * AWS SDKs
    * AWS CloudFoundation
    * AWS Elastic Beanstalk
    * AWS OpsWorks
* Third Party
    * Troposphere
    * Terraform by Hashicorp
    * Ansible by RedHat
    * SaltStack

### Relaizing the power of AWS
* Agility & Flexibility
* Lowest time to market
* Fasr, Efficient deployments
* Self-Healing
* Scalable

## AWS Elastic Beanstalk
* Application management platform
* Provides easy Entry
* Choice of; Java, .NET, Node.js, Docker, and more.
* Simply upload Application
* Ideal for developers
* Automatically handles
    * Capacity provisioning
    * Load Balacing
    * Auto Scaling
    * Monitoring
    * Deployment

## AWS CloudFoundation
* Template-Based infrastrucutre managment
* Declarative progamming
* Offers access to full breadth of AWS
* Store in source Control
* Write once, deploy many
* Library for common architectures
* No imposed model

## AWS WAF (Web Application Firewall)
* Layer 7 content filtering
* Supports rules to block/allow/count
* Integrates with Amazon CloudFront
* Protects agasint; SQL Injection, Cross-site Scripting (XSS)
* Block based on; IP Addresses, HTTP Headers/Body, URI String
* Rate Limiting per client IP
* MAnaged Rules for common threats; OWASP, Bots, Common Vulnerabilities and Exposures (CVE)

## AWS Shield
* DDoS Protection Service
* Standard; UDP reflection, SYN Floods, SSL Renegotiation, Slow loris attacks, Available for free to everyone.

### AWS Shiekd Advanced
* Additional detection/mitigation
* Newar real-time visibility
* Integrates with AWS WAF (Web Application Firewall)
* Access to DDoS Response Team.
