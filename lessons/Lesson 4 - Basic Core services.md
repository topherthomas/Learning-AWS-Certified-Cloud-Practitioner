# Lesson 4

## Settings the Stage

### Well-Architected Infrastrucutre
* Reliable
    * Fault Tolerance
    * High Availability
    * Durability
* Secure
* Performant
* Cost-Effective
* Operationally Excellent
    * Monitored
    * Automated
    * Effective Processes

### Example Architecture

## Networking with Amazon VPC
* Logically isolated Networks
* Created per Account per Region
* Spans a single Region
* Can use all Availability Zones within one region
* Can peer with other VPCs
* Internet and VPN Gateways
* Numerous Security mechanisms

### Three - Tier Arch
1. Load Balancing Tier
2. Application Tier
3. Database Tier

### Subnets Enable
* Security via isolation
* High-Availability
* Fault-Tolerance
* Performance

## Routing and Firwalls

### Routing
### Network Access Control Losts (NACL)
### Security Groups

## DNS, VPN, and Direct Connect

### Amazon Route 53
* Register Domains
* USe AWS nameservers
* Public and private DNS zones
* Automated via API
* Health Checks
* Different routing methods
    * Latency
    * Geographic
    * Failover
    * Weighted Sets

### AWS Hardware VPN
Hardware-enabled IPSEC VPN Tunnel

### AWS Direct Connect
AWS Direct connect point of Presence (POP).
Dedicated 1 or 10 Gbps fiber connection.

## Use Cases: Network Security

## Amazon Elastic Compute Cloud (EC2) Part 1
* Virtual Machones (instances)
* Linux or Windows
* Xen or Nitro hypervisioor
* Bare metal is available
* Combinations of CPU, memory, disk, io
* Launch one to thousands
* Different billing models
* Hourly fee includes OS license
* AWS Marketplace offers canned solutions

### Amazon EX2
* Physical Host
    * Rack, Chassis, ect...
    * Intel Xeon
    * Hypervisor (Xen/Nitro)
    * Dedicated Host (optional) where a single host is allowed for a single customer

### Amazon Machine Image (AMI)
* Bit-for-bit copy of root
* Choice of OS
    * Windows, Linux, FreeBSD
* Find AMIs:
    * AWS-Provided
    * Trusted publishers
    * Community
    * AWS Marketplace

### Launching Instance
1. Choose AMI
2. Launch Instance 
3. Install, configure, ect,
4. Create private AMI

## Amazon Elastic Compute Cloud (EC2) Part 2

### Use Cases
* Enterprise applications
* Web Servers
* Relational databases
* NoSQL datastores
* Video Transcoding
* Batch Processing
* Container orchestation
* Code repos/Build servers

### Best Practices
* Treat as disposable
* "Immutable infrastructure"
* Treat logs as streams
* Leverage roles
* Automate deployments
* Monitor with CloudWatch
* Enable scaling and self-healing with auto-scaling

## Demo: Launching an EC2 instance

### Block Storage vs Object storage
* Changing bytes in a GB File.

* Compare
    * Update volume blocks with relevant bits | Upload entire File
    * Use more efficient protocol | Uses HTTP for Transfer
    * CAN be mounted | Cannot be mounted
    * Best for Random IO | No Random IO

* Static objects, Object Storage. If it changes frequently, Block Storage.

## Amazon Elastic Block Store (EBS)
* Data independent from instance
* Connected over network
* Pay for provisioned storage
* Exist in single Availability zone
* Point-in-time snapshots
* Can Detach
* Can attach to different instance
* Can be encrypted (AES-256)
* Can be used in RAID or LVM
* If you need a file system with lot of random.

## Elastic load balancing
* Distributes Requests/traffic
* Spans region, use every AZ
* Inherently secure, resilient, scalable
* support health checks
* Integrates with Auto Scaling.
* Integrates with Route 53.
* Three Types
    * Classic
    * Application
    * Network

### Classic
* Defines Listener port
* Registers Instances 
* Forward to port
* Offload SSL, Customize securtiy policies
* Supports health checks
* inherently scalable & self-healing

### Application
* Defines Listern Port
* Create Target Groups with instances inside
* Rules so certain URIs or domain names gets router to certain target group.
* Dynamic port mappings to allows one instance ot recieve on different ports.

### Network
* Listner on port
* Target Groups
* Forward to port
* Connections to static IPs can be spread across instances.
* Improves latency and handles very long running connections.

## Amazon Simple Storage Service (S3)
* Object Storage
* Buckets and objects
* Cluster spans region
* Durable to lose of 2 AZs
* Server side encryption
* Simulate strucutre system with "/"

### Use Cases
* Static HTML
* CSS, JS
* Images
* Audio & video
* PDF, eBooks
* Software downloads
* Log collection
* Data lakes
* S3 provides durable object storage for static assets

## Demo: Using Amazon S3

