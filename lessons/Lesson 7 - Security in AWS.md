# Lesson 7

## The Shared Responsibility model

### AWS is responsible for
* Physical Security; Facilities, Data Centers, Edge Locations, Rack and Chassis
* Network
* APIs
* Hypervisor
* Managed Services; Storage, Databases

### Customer is responsible for
* Operating System
* Network & Firewall Configuration
* Identity and access; Credentials, Permissions
* Applications
* Data
* Encryption; At Rest, In Transit

### Controls
* Inherit; Physical, Environment
* Shared; Patch Managment, Configuration managment, Education
* Customer Specific; Application, Zone Security

## Identity and Access Management (IAM) Part 1
* Authentication
* Authoriation via policies
* Users
* Groups
* Password Policy
* Multifactor Authentication

### Users and Groups
* Users
    * Are Created and exit within IAM service
    * Login to managment console
    * Can have long-term access keys
    * Can enable per-user MFA device
* Groups
    * Cannot be nested

### Credential Types
* Credential -> User for
    * Email + Password -> Master Account (root) access
    * Username + Password -> AWS Web Console
    * Access Key + Secret Key -> CLI, SDK

### Policies
* Determine Authorization (permissions)
* Written in JSON
* Policy Types
    * Managed Policy
        * AWS Managed
        * Cutomer Managed
    * In line policy
* Create plocies via
    * Generator
    * Hand Written Policies
* Evaluation Logic:
    * Defaults to implicit deny
    * Explicit deny
    * Explicit Allow

### What not to do
* Embed in
    * Code
    * Environment Variables
* Share with
    * Third Parties
    * Hundreds of enterprise users
    * Millions of web users

### Roles
* Use temporary credentials
* Delegate Permissions to:
    * EC2 Isntance
    * AWS Service
    * A user (elevate privs)
    * Separate account
        * In house
        * Third Party

## Identity and Access Management (IAM) Part 2

### Example 1
* Can assign roles to an instance.
* Apply policy to role.
* Provide temporary credentials which the EC2 instance can retrieve.
* Use Temp Creds to authenticate.

### Roles for Cross Account Access.
* Create roles with cross access. Rolling Temporary credentials can then be given when needed to allow access.

### Federated Users
* Org Users
    * Leverage existing directories
        * LDAP
        * Active Directory
    * Temp Creds
    * Single Sign On
* Web/Mobile Application Users
    * Apps bypass backend APIs/Proxies

### AWS New Account First steps
1. Enable CliudTrail
2. Create an admin user IAM
3. Enable Multi-Facotr Auth on root account
4. Enable cost & usage Report
5. Log Out of root
6. Log in with Admin
7. Create new users and groups

### Best Practices
* Root Credentials
    * Email + Password
    * Protect at all costs
    * Do not use for Day-To-Day
* Follow principle of least privilege
* Rotate access keys
* Enable Multi-Facotr Authentication (MFA)
* Monitor with CloudTrail.

## Demo: Managing Users Groups, and Roles


## AWS Organizations
* Eases management of multiple AWS Accounts
* Automate creation of AWS Accounts.
* Service Control Policies (SCPs) control service use.
* Types
    * Org Units: Help organize accounts
    * AWS Account: Isolate resources and controls access
    * SCPs: restrict service usage

### Consolidated and Detailed Billing
* Consolidated Billing
    * One bill, many accounts
    * Aggrefate volume pricing
    * Reseved instance apply to all accounts
* Detailed Billings
    * Published to S3 Bucket
    * Import into spreadsheet
    * Filter/Sort by service, tag, ect...

## AWS Assurance Programs
* Certifications/Attestations
    * Performed by third-party independent auditors
* Laws/Regulations/Privacy
* Alignments/Frameworks

### Global Assurance Programs
* Cloud Security Alliance

### U.S. Assurance Programs
* HIPPA
* FISMA
* PSI

### Hippa Compliance
* Designed to secure Protected Health Information
* AWS not "Directly" certified.
* Requirements map to FedRAMP and NIST 800-53
* AWS provides Business Associate Addendum

### PCI DSS COmpliance
* Desinged to protect
    * Cardholder Data
    * Sensitive Authentication data
* Applies to business that
    * store/process/transmit such data  
* Customers responsible for card data environment
* AWS provides Attestation of Compliance (AOC)

### Achieving Compliacne
* Customer works towards compliance
* AWS Provides necessart documentation
* Know who is responsible.
    * AWS for physical controls
    * Cusstomer for logical controls
* Know the controls you inherit.
* Know the service in scope. (Not all services are in scope of all compliance)
* Speak with your account manager.

## Key Sevices for Auditing and Compliance
###AWS config
* Resource Inventory
* Configuration history
* Change notifications
* Determine compliance against rules
* Enables
    * Compliance auditing
    * Security analysis
    * Change tracking
    * Troubleshooting

### AWS Service Catlog
* Manage catalogs of approved IT services
* Achieve consistent governance
* Customer defines
    * Profolios
    * Products
        * Defines CloudFormation Templates


### AWS Artifact
* Access reports/details of >2500 security controls
* On-demand access to AWS security and compliance dpciments
* Demonstrate security and compiance of your AWS Environment.

### AWS Cloud Trail
* Records all calls made to AWS APIs
* Delivers log files to S3 Buckets
* Includes
    * Identity
    * Source IP
    * Request/Response details
* Does not record
    * OS System Logs
    * Database queries

### Encryption and Key Managment
* Many Services offer encryption like
    * S3, EBS, RDS, Glacier, SQS
* AWS Key Management Service (KMS)
    * Fully Managed Service
    * Create/Manage encryption keys
    * Integrates ith many other services
    * Multi-Tenant software backed by HSMS.
* AWS CloudHSM
    * Single-Tenant hardware security modules
    * FIPS 140-2 Level 3 Validated
    * On-demand, no upfront costs
    * Can enable
        * SSL Offloading
        * Private key storage.security
        * Transparent Data Encryption

## Use Case: PCI DSS Regulated Workload


## Vulnerability and Penetration Testing
* Permission is Required!
* Must request permission via root credentials
* Identify the instances to be tested
* Specify start and end date/time
* AWS does **not** permit testing of
    * m1.small
    * t1.micro
    * t2.nano
* AWS policy permits testing of
    * EC2
    * RDS
    * Aurora
    * CloudFront
    * API Gateway
    * Lambda
    * Lightsail
    * DNS Zone Walking
