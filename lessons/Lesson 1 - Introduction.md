# Lesson 1: Cloud Practitioner Certification

## Resources
TODO

## The Exam
- Domains
    - Cloud Concepts
    - Security
    - Technology
    - Billing and Pricing
- Response Types
    - Multiple Choice
    - Multiple Response
- Register
    - https://www.aws.training/
- Approx. 65 Questions
- 90 Minutes to complete
- Unanswered questions are incorrect
- No penalty for incorrect answers
- Review questions beofre submittal
- Only need to pass overall

### Exam Strategy
- First pass: Choose what you know you know
- Second pass: process of elimination
- Review before submitting
- It's OK to guess

### Taking the exam
- Take two forms of ID
- Take proof of exam registration (email on smart phone)
- Leave all belongings in locker
- Can use scratch paper
- No ability to research

## Documentation and Study Material



