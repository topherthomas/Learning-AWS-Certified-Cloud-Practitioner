# Lesson 8

## Compute Pricing Part 1

### EC2 Ondemand Pricing
* No long-term commitments
* Transform large, fixed costs, into smaller, variable costs
* Fees include OS License
* Per hour billed hour forward
* Per second minimum 60 seconds

### EC2 Reserved Isntances
* Up to 75% discount vs On-Demand.
* Provide capacity reservation.
* 1-year or 3-year term
* Attributes
    * Instance Type
    * Platform
    * Tenancy
    * Availabiltiy Zone (Optional)

### Reserved Instances Types
* Standard RIs
    * Up to 75% of on-demand
    * Best for steady-state usage
* Convertible RIs
    * Can change attributes of instance
    * Up tp 54% off on-demand
    * Best for steady-state usage
* Scheduled RIs
    * Available during time window
    * Best for predictable, recurring schedule

## Compute Pricing Part 2
### EC2 Spot Pricing
* Gain Discounts on spare capacity
* Save up to 90% vs on-demand
* Price
    * Set by isntance type + AZ
    * Determine by supply/demand
* Spot instances can be interrupted
* Great for
    * Apps with flexible start/end times
    * Development/test environments
* Example Use cases:
    * Image rendering
    * Video transcoding
    * Analytics
    * Machine Learning
* Integrated with
    * Amazon EMR
    * Auto Scaling
    * Elastice Container Service (ECS)
    * CloudFormation

### Amazong Lambda Pricing
* Charaged per GB per 100ms
* Charaged per 1M Requests
* Free Tier
    * 1M Request/mth
    * 400k GB-Seconds/mth
* Additional charages
    * Bandwith
    * Amazon S3

### Data Transfer Pricing
* Inbound generally free
* S3 to CloundFront is free
* Outbound to internet
    * $/GB/mth
    * Applies cross-region traffic
    * Tiered Pricing
        * 10/40/100TB
* Cross-AZ traffic $/GB/mth
* VPC peering $/GB/mth

## Database Pricing

### RDS
* Price determined by; instance type, DB Engine (License), Reserved instance discount
* Multi-AZ deployments 2x $
* Storage; $/GB/mth, $/provisioned throughput

### Dynamo DB
* Charaged for provisioned throughput
    * $/Write capacity unit
    * $/Read capacity unit
* Charaged fro storage consumed
    * $/GB/Mth

## Storage Pricing

### EBS
* $/GB/mth of provisioned storage
    * General purpose SSD
    * Provisioned IOPS storage
    * Magnetic volumes
* Additonal Costs
    * $/Provisioned IOPS
    * EBS snapshots to Amazon S3

## S3 Pricing
* $/GB/mth of consumed storage
* Additional cost for request
    * PUT, COPY, POST, LIST $/1000
    * GET $/1000
* Storage classes
    * Infrequent access
    * Glacier

