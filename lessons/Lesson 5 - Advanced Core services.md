# Lesson 5

## Advancing the Stage


## Amazon CloudWatch
* Collects Metrics
* Stores metrics for 2 weeks
* Accessible bvia API
* Unique Metrics ser per service
* Customer metrics ($/Metric)
* EC2
    * Default 5 min interval
    * Detailed 1 min ($/instance)
    * Reported by hypervisor
* ELB
    * Default 1 min
* RDS
    * Memory, connections, disks
* DynamoDB
    * Read/Write throughput

### Instrmentation
* Are we over or under provisioned
* What is current demand/load?
* Where are the bottlenecks?
* How is this _ performing?
* Are any resources idle?

Answer Monitor!

### Amazon CloudWatch Alarms
* Triggered on breach of threshold
* Does not necessarily signial emergency
* Can Trigger
    * Auto Scaling
    * Termination
    * Reboot
* Up tp 5000 Alarms per account

### Amazon CloudWatch Logs
* Collect logs by streaming
* Configure agent on instances
* Collect Route 53 DNS Queries
* Monitor CloudTrail Events
* Default retention indefinite (Will hold the logs forever unless told otherwise)
* Can Archive to Amazon S3
* Stream 10 Amazon Elasticseatch
* Process with Lambda (In near real time)

#### Searching and Filtering log data
* Search using specific syntax
* Create Metric Filters
    * Counts as custom metric
    * Create alarms such as
        * Number of 404s
        * Bytes transfered
        * Customer Conversions
        * Number of Exceptions.

## AWS Auto Scaling
* Replace failed instances.
* Change capacity according to load
* Maintain fixed size fleet
* Works with Amazon cloud watch
* Events
    * Sns
    * Lambda

### Example
1. ASG of multuple EC2 Instances.
2. Cloud Watch monitors the ASG and does metrics check
3. If CPU > 80% for 5 periods of 1 minute, Auto Scaling adss 100% (doubles) the EC2 Instances.
4. Similar check for down scaling.

## Amazon CloudFront
* Caches content at Edge Location
* Static & Dynamic content
* Customizable Cache behavior (default time to live. Dynamic conent should be cached for X).
* Custom Domain name (CNAME or ALIAS)
* Cusstom SSL certificates
* RTMP and HLS streamin.

### Notes
* Distribution (Web or Streaming)
* Give origins (S3, Load Balancers -> EC2)

### Example
* Cloud Front on West Coast. Dynaic Connents comes from ELB and static from S3.
* Leverage DNS, in another region like Spain. Frist person who access your site would have to load from source. It is then stored in that cache.

## AWS Lambda
* Serverless Infrastrucutre
* Great for
    * Scheduled Tasks
    * MicroServices
    * Event Handlers
* Pay for compute time per 100ms
* Create Functions
    * Inline Editor
    * Zip File
* Invoke functions
    * CLI or SDK
    * Events
* ASW Handles
    * Infrstructure
    * Deployment
    * Scaling

## Demo: Creating and Invoking AWS Lanmda Functions


## Amazon Relational Database Service (RDS)
* Choice of
    * MySQL
    * SQL Server
    * Oracle
    * PostgresSQL
    * MariaDB
    * Amazon Aurora
* Reduced operational burden
* Focus on Application
* Read Replicas
* Automated
    * OS & DB Installation
    * OS Patches
    * DB Engine minor updates
    * Backups
    * Failover

### Multi-AZ Deployments
* High Availability
* Physically Distinct
* Synchronous Replication
* Automatic Failover
    * Loss of network
    * Compute failure
    * Storage failure
* RTO/RPO in minutes
* Production best practice

### Backups: Automated
* Performed once per day
* Yoiu control backup window
* Retained up to 35 dyas
* Point-in-tinme restore
* Deleted along with instance.

### Backups: Manual
* Performed at any anytime
* Can copy to other regions
* Retained indefinitly

### Amazon Aurora
* Compatible with
    * MySQL 5.6
    * PostgresSQL 9.6
* 3-5x performance increase
* Storage up to 64TB
    * Auto-Scaled
    * Six Copies across three AZs.
* Up to 15 read replicas
* Aurora Multi-Master
* Aurora Serverless

### Database Migrations
* Dump and import
* Backup/Restore
* AWS Database Migration Service
    * Supports widely used DBs
    * Hererogeneous/Homogeneous DBs
    * Virtually zero downtime
    * Schema conversion tool
    * Consolidate DBs
    * Continuous Replication

### AWS Manages
* Power & cooling
* Network
* Chassis
* CPU, RAM, HDD
* Operating System
* Database
* Backups

## Amazon DynamoDB
* NoSQL Data Store
* Exclusively backed by SSD
* Single-Digit millisecond response
* Build-In security, resilience
* Replicated across multiple AZs.
* No Limits to storage or throughput
* Provisioned throughput
    * Reads
    * Writes
    * Can auto-scale

### Amazon DynamoDB Tables
* Tables, items, attributes
* No Joins/Relationships
* Schema-less
* Key value & documents
* Unique primary key required
* Secondary indexes
* No table size limit
* 400KB item size limit
* Item-level Time-To-Live (TTL)

### Use Cases
* Ad impression/clicks
* Gaming leaderboards
* Shopping carts
* Operational state/history

## Amazon Glacier
* Archival storage
* Lower cost vs S3
* Write archives
    * Transition from S3
    * Direct upload.
* Download via retrieval request
    * 3-5 hour wait
    * $ for faster retrevial

### Life Cycle Rules
* After X Days, Archove from S3 to Glacier
* After X Years, Permanently delete from Glacier.

## Amazon Redshift
* Petabyte scale data warehouse
* Fully managed like RDS
* Fork of PostgresSQL 8.0.2
* SQL Compliant
* Connect with JDBC/ODBC
* Parallel Queries
* Ideal for OLAP & BI Applications


## Use Case: E-Commerce
