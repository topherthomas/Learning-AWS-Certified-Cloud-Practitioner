# Lesson 2
NIST Cloud Computing Definition
* On-demand self-service
* Broad network access
* Resource Pooling
* Rapid elesticity
* Measured Service

## Introuction

### Service Models
* Infrastructure as a Service (IaaS)
* Platform as a Service (PaaS)
* Software as a Service (SaaS)

### Deployment Models
* Private cloud
* Community Cloud
* Public Cloud (what AWS is)
* Hybrid Cloud
    * Leveraging AWS for certain needs, and also running some storage on primises.

## Business Needs and Cloud Solutions
* Resiliency
   * Self Healing, Outage recovery, ect...
   * AWS has load balacing and scaling
* Security
* Durability
* Performance
* Cost-effectivness
* Scalability
* Automation

### Process & Workflow
* Agile
* Flexible
* Efficient
* Secure

### Buisness
* Secure, reliable data center
* Low CapEx
* No long-term commitments
* Fast Time to market

### AWS
* Compute
* Stoage
* Application Service
* Datastores
* Analytics
* Networking
* Development/Deployment

## AWS Cloud Value Proposition
* On-demand resources
* Pay as you go
* No long-term commitments
* Highly Automated
* Managed Services
    * Inherit high-availability, security, durability 

### We Benifit
* OpEx (operating expenses) over CapEx (capital expenditures)
* Gain flexibility, agility
* Immediate scalability
* Lower time to market
* Lower variable costs
* Lower upfront costs
* Easier cost allocation
* Stop running data centers.
