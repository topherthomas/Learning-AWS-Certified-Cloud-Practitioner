# Lesson 3

## AWS Global Infrastructure

### Choosing a Region
* Available services and features
* Cost of services
* Latency, proximity to users
* Disaster recovery
* Security & compliance

### Availability Zones
* Multple zones in a region, connected with a fiber cable
* ex: `us-west-2a` `us-west-2b`

### Edge Location
* Edge locations power content delivery services and DNS. 
* Some located outside regions. 
* No direct access to edge locations

## AWS Managment Console (Demo)
* Core services are on the left
* Shortcut bar at the top per user
* 

